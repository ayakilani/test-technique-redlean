export const environment = {
  production: false,
  announcements: [
    {
      id: 1, title: 'Lazaretto', description: 'Ultra LP (180G)[180G]', price: 17.34, state: 'Comme neuf', shipping_cost: 5.15,
      seller_comment: 'NEUF-Envoyee par avion des USA SOUS 24hrs - Livraison en moyenne de 7 a 14 jours ouvers - ' +
        'Tous les DVDs = Region 1 (USA/CA) -Excellent sevice clientele en francais -Achetez en toute confiance !',
      category: 'Vinyle', seller_picture: '../assets/imgs/seller-avatar.jpg', total_comments: 10, picture: '../assets/imgs/Lazaretto.jpg',
      rank: 80.5
    },
    {
      id: 2, title: 'KLIM Impact', description: 'Casque Gamer USB', price: 22.49, state: 'Comme neuf', shipping_cost: 0,
      seller_comment: 'NEUF-Envoyee par avion des USA SOUS 24hrs- Livraison en moyenne de 7 a 14 jours ouvers - Son 7.1 Surround' +
        'Isolation - Audio Haute Qualité + Fortes Basses - Micro Casque Gaming Jeux Vidéo pour PC PS4',
      category: 'Casque', seller_picture: '../assets/imgs/userx-avatar.jpg', total_comments: 12, picture: '../assets/imgs/klim-casque.jpg',
      rank: 50.3
    },
    {
      id: 3, title: 'Garmin Forerunner 30 ', description: 'Montre GPS de Course à Pied', price: 31.97,
      state: 'Comme neuf', shipping_cost: 0, seller_comment: 'NEUF- Livraison en moyenne de 7 a 8 jours ouvers- GPS intégré' +
        ' qui enregistre votre vitesse de course,la distance et votre position.', category: 'Montre connectée',
      seller_picture: '../assets/imgs/user-default.png', total_comments: 15, picture: '../assets/imgs/garmin-for-runner.jpg',
      rank: 65.0
    },
    {
      id: 4, title: 'VicTsing Tracker', description: 'Bracelet Connecté Intelligent IP68 Waterproof Fitness', price: 17.34,
      state: 'Comme neuf', shipping_cost: 5.15,
      seller_comment: 'NEUF- Livraison en moyenne de 7 a 8 jours ouvers- Compatibilité large et longue durée de vie de la batterie,' +
        'La batterie rechargeable haute performance garantit jusqu à 15 jours d autonomie sur une charge complète d environ 1 heure -' +
        ' cardiofréquencemètre, Surveillant d activité automatique 24 heures- Écran OLED réactif et étanche à l eau IP68,Avec le contrôle' +
        ' multi - touche.', category: 'Montre connectée', seller_picture: '../assets/imgs/user-default.png', total_comments: 3,
      picture: '../assets/imgs/victsing-tracker.jpg', rank: 80.9
    },
    {
      id: 5, title: 'Sony Xperia XA1 Verre Trempé', description: 'Film protection en Verre Trempé 9H ',
      price: 22.49, state: 'Comme neuf', shipping_cost: 0, seller_comment: 'NEUF- Livraison en moyenne de 3 jours ouvers- ' +
        ' Ecran protecteur ultra résistant - vitre de protection de 0,33 mm d épaisseur - Facile à poser - degré de dureté de 9H' +
        ' pour une résistance maximale même contre des éraflures et des rayures ', category: 'Accesoires smarthphone',
      seller_picture: '../assets/imgs/user-default.png', total_comments: 23, picture: '../assets/imgs/sony-verre.jpg',
      rank: 60.5
    },
    {
      id: 6, title: 'TUCCH Housse', description: 'Portefeuille iPhone 8 ', price: 22.49, state: 'Comme neuf', shipping_cost: 0,
      seller_comment: 'NEUF-  Livraison en moyenne de 3 jours ouvers- Antichoc TPU Coque Cuir Synthétique Etui de Protection avec' +
        ' [Béquille] [Fentes pour cartes] [Fermoir Magnétique] pour iPhone 7 (4,7 pouces) - Noir', category: 'Accesoires smarthphone',
      seller_picture: '../assets/imgs/user-default.png', total_comments: 7, picture: '../assets/imgs/tucch-housse.jpg',
      rank: 10.5
    },
    {
      id: 7, title: 'SAFEMORE Multiprise Parasurtenseurs', description: 'Parafoudre 7 Prises ', price: 22.49, state: 'Comme neuf',
      shipping_cost: 0, seller_comment: 'NEUF-  Livraison en moyenne de 3 jours ouvers-2 Ports Smart USB (5V / 2.1A total) -' +
        '2 ports USB universels, avec puce à puce intelligente pour identifier le taux de charge de votre smartphone, ' +
        'appareil photo numérique, ventilateur usb, écouteur bluetooth,' +
        ' moniteur bébé. 7 Sorties AC Espacées - Prolonge la durée de vie de vos appareils, A protection contre les surcharges,' +
        'y compris le PORT SAFE protégera les enfants d une électrocution éventuelle.', category: 'Électricité › Multiprises',
      seller_picture: '../assets/imgs/user-default.png', total_comments: 47, picture: '../assets/imgs/safemore-multiprise.jpg',
      rank: 70.9
    },
    {
      id: 8, title: 'Intex 58446NP Piscine Gonflable', description: 'Vinyle Bleu Cristal 1.68m x 38cm ', price: 14.50, state: 'Comme neuf',
      shipping_cost: 0, seller_comment: 'NEUF-  Livraison en moyenne de 20 jours ouvers- Piscine Bleu cristal- Dimensions: 1.68m x 38cm' +
        '-Fabriqué en vinyle robuste de haute qualité - ' + 'Kit de réparation inclus -Garantie : 3 mois -Installation' +
        'facile et de dimensions idéales pour une utilisation occasionnelle sans prendre trop de place sur la terrasse ',
      category: 'Piscines gonflables', seller_picture: '../assets/imgs/user-default.png', total_comments: 47,
      picture: '../assets/imgs/piscine-gonflable.jpg',
      rank: 30.5
    },
    {
      id: 9, title: 'EocuSun Sac', description: 'Grande Sac à Dos de Plage  ', price: 12.99, state: 'Comme neuf',
      shipping_cost: 5, seller_comment: 'NEUF-  Livraison en moyenne de 20 jours ouvers - Sac de grande capacité' +
        ' -Taille: 55cm (H) x 38cm (L) x 19cm (W) Sac de plage' + ' pour mettre les jouets, serviettes, vêtements, coquillages,' +
        'pagaies, balles de plage, lunettes de soleil' + ' lorsqu on va aller sur la plage - Utilisation facile Sac de natation' +
        'est doté d une poche à cordon pour protéger entièrement vos objets.', category: 'Sacs de natation',
      seller_picture: '../assets/imgs/user-default.png', total_comments: 47, picture: '../assets/imgs/sac-natation.jpg', rank: 60.5
    },
    {
      id: 10, title: 'Kingston - SDC10G2/128GB ', description: 'Carte MicroSD - 128 Go - Adaptateur SD', price: 50.80, state: 'Comme neuf',
      shipping_cost: 10, seller_comment: 'NEUF-  Livraison en moyenne entre 10 et 15 jours -Petit format- Durable pour protéger' +
        'vos photos et vidéos - Capacités de 8Go à 128Go', category: 'Électricité › Multiprises',
      seller_picture: '../assets/imgs/user-default.png', total_comments: 47, picture: '../assets/imgs/sdc.jpg', rank: 70.9
    },
    {
      id: 11, title: 'Odomètre de vélo Ohuhu', description: 'Parafoudre 7 Prises ', price: 22.49, state: 'Comme neuf',
      shipping_cost: 0, seller_comment: 'NEUF-  Livraison en moyenne entre 2 jours - Lumières de retour - allumer automatiquement' +
        'entre 18h00 -06: 00 - Permettre lès cyclistes disponibles de lire les données de cyclisme sur le compteur de vitesse -' +
        'Fonctions multiples - calculate le vitesse d équitation, la distance totale, la distance parcourue, horloge, la vitesse moyenne,' +
        'l information de la concurrence ...', category: 'Électronique › Compteurs vélo', seller_picture: '../assets/imgs/user-default.png',
      total_comments: 47, picture: '../assets/imgs/ohuhu.jpg', rank: 50.4
    },
  ],
};
