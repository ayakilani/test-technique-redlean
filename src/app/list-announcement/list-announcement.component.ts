import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-list-announcement',
  templateUrl: './list-announcement.component.html',
  styleUrls: ['./list-announcement.component.css']
})
export class ListAnnouncementComponent implements OnInit {
  total_cart: number;
  searchTerm: any;
  _announcements: any[];
  chosen_announcments: Array<any> = [];
  announcement: Object = {};
  is_free: boolean;
  announcements: Array<any> = environment.announcements;
  limit = 6;


  constructor() {
    this._announcements = this.announcements;
  }

  ngOnInit() {
    this.announcements.forEach(element => {
      if (element.shipping_cost > 0) {
        element.is_free = false;
      } else {
        element.is_free = true;
      }
    });
    this.chosen_announcments = JSON.parse(localStorage.getItem('panier'));
    if (!this.chosen_announcments) {
      this.chosen_announcments = [];
    }
    this.total_cart = this.chosen_announcments.length;
    console.log('nombre d article au panier', this.total_cart);
  }

  // go to details announcement
  openDetails(item) {
    this.announcement = item;
    console.log(this.announcement);
  }

  // add a desired item to cart
  addToCart(announcement) {
    this.chosen_announcments.push(announcement);
    localStorage.setItem('panier', JSON.stringify(this.chosen_announcments));
    this.total_cart = JSON.parse(localStorage.getItem('panier')).length;
    console.log('nombre d article au panier', this.total_cart);
  }

  // check if an object is empty
  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  setFilteredItems() {
    this.announcements = this.filterItems(this.searchTerm);
    if (this.searchTerm === '') {
      this.announcements = this._announcements;
    }
  }

  filterItems(searchTerm) {
    return this._announcements.filter((item) => {
      return JSON.stringify(item).toLowerCase().includes(searchTerm.toLowerCase());
    });
  }

  // show more announcement
  loadMore() {
    this.limit = this.limit + 5;
  }

  normalizeRank(rank) {
    return Math.floor(rank / 20);
  }
}
