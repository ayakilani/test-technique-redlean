import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAnnouncementComponent } from './list-announcement/list-announcement.component';

const routes: Routes = [
  { path: '', redirectTo: 'list-announcements', pathMatch: 'full' },
  { path: 'list-announcements', component: ListAnnouncementComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
